# The Matrix

---

**Q/W** Change **Manual/Automatic** modes  
While on manual mode choose the active column with a number key (1 to 9)

---

Recrear una animaci� amb c# per consola.  
Donat el seg�ent codi:  
https://gitlab.com/eruesga-xtec/protogameengine  

Repte 1: Hem de imprimir una matriu amb 0's. L'animaci� recrear� la pluja de car�cters de la pel�l�cula Matrix. Una lletra aleatori caur� en cada frame una fila fins arribar al final. La columna on es generi tamb� ser� aleatori.  
 
Repte 2: (Bonus) Les lletres cauran per la columna que l'usuari ha seleccionat. L'usuari pot indicar la columna de sortida amb el numero de columna o amb un cursor mogut per fletxes.