﻿using GameTools;
using System;

namespace MyFirstProgram
{
    class TheMatrixSimulator : GameEngine
    {
        MatrixRepresentation representation;
        Random rand;
        public bool Manual { get; set; } = false;
        public int PressedNum { get; set; } = 1;
        public int SelectedColumn { get; set; } = 0;
        public int MatrixHeight { get; set; } = 20;
        public int MatrixWidth { get; set; } = 20;
        public int ManualMaxColumn { get; set; }
        private ConsoleKeyInfo keyinf;

        protected override void Start()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            rand = new Random();
            FrameRate = 12;
            representation = new MatrixRepresentation(MatrixHeight, MatrixWidth);
            ManualMaxColumn = (MatrixWidth < 9) ? MatrixWidth : 9;
        }

        protected override void Update()
        {
            CheckMode();
            MakeCharacters();
            representation.printMatrix();
            ScrollCharacters();
        }

        //Changes mode, Q for manual, W for automatic
        private void CheckMode()
        {
            if (Console.KeyAvailable)
            {
                keyinf = Console.ReadKey(true);
                Manual = keyinf.Key == ConsoleKey.Q ? true : Manual;
                Manual = keyinf.Key == ConsoleKey.W ? false : Manual;
            }
        }

        //scroll characters down
        private void ScrollCharacters()
        {
            for (int i = representation.TheMatrix.GetLength(0) - 1; i > 0; i--)
            {
                for (int j = representation.TheMatrix.GetLength(1) - 1; j >= 0; j--)
                {
                    representation.TheMatrix[i, j] = representation.TheMatrix[i - 1, j];
                    representation.TheMatrix[i - 1, j] = ' ';
                }
            }
        }

        //output new characters
        private void MakeCharacters()
        {
            if (!Manual)
            {
                for (int i = 0; i < representation.TheMatrix.GetLength(1) / 2 - 1; i++)
                {
                    representation.TheMatrix[0, rand.Next(0, representation.TheMatrix.GetLength(1))] = RandomChar();
                }
            }
            else
            {
                if (Console.KeyAvailable)
                {
                    keyinf = Console.ReadKey(true);
                    PressedNum = (int)keyinf.Key - (int)ConsoleKey.D0;
                    if (PressedNum >= 1 && PressedNum <= ManualMaxColumn)
                    {
                        SelectedColumn = PressedNum - 1;
                    }
                }
                representation.TheMatrix[0, SelectedColumn] = RandomChar();
            }
        }

        private char RandomChar()
        {
            //33 to 125 is an ASCII range with fairly readable characters
            return Convert.ToChar(rand.Next(33, 125));
        }
    }
}
